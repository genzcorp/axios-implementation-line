'use strict';
const qs = require('qs')
const express = require('express');
const axios = require('axios')
var bodyParser = require('body-parser')
const querystring = require('querystring');
const instance = axios.create({
  baseURL: 'https://notify-api.line.me',
  timeout: 5000,
  headers: { 'Authorization': 'Bearer ' + process.env.TOKEN, contentType: 'application/x-www-form-urlencoded' },


});
// create LINE SDK config from env variables
var jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

// create LINE SDK client
// const client = new line.Client(config);

// create Express app
// about Express itself: https://expressjs.com/
const app = express();
app.use(jsonParser)
// register a webhook handler with middleware
// about the middleware, please refer to doc
function getQueryString(data = {}) {
  return Object.entries(data)
    .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    .join('&');
}

app.post('/webhook', (req, res) => {

  const data = qs.stringify(req.body)
  const params = new URLSearchParams()
  params.append('message', JSON.stringify(req.body))
  instance.post('api/notify', params).then(res => {
    console.log(res.data)
  }).catch(err => {
    console.log(err)
  })
  res.send({ status: 'ok' })
})
// listen on port
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`listening on ${port}`);
});
